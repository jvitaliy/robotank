package simplerobo;
import robocode.HitByBulletEvent;
import robocode.HitWallEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

import java.awt.*;

import static robocode.util.Utils.normalRelativeAngleDegrees;

public class Firedragon extends Robot {
    double gunTurnAmt; // How much to turn our gun when searching
    double fieldHeight = 0.0;
    double fieldWidth = 0.0;
    double step = 50.0;
//    int scannedX = Integer.MIN_VALUE;
//    int scannedY = Integer.MIN_VALUE;
    @Override
    public void run() {
        setBodyColor(new Color(10, 15, 41));
        setGunColor(new Color(50, 50, 20));
        setRadarColor(new Color(200, 200, 70));
        setScanColor(Color.white);
        setBulletColor(Color.white);
        fieldHeight = getBattleFieldHeight();
        fieldWidth = getBattleFieldWidth();
        gunTurnAmt = 360; // Initialize gunTurn to 10
        turnGunRight(360);

        while (true) {
            turnGunRight(90);
            if (getX()>10 && getX()< fieldWidth-10 && getY()>10 && getY()< fieldHeight-10){
                ahead(step);
            }else{
                back(step*3);
                turnLeft(45);
            }

        }
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
        double absoluteBearing = getHeading() + e.getBearing();
        double bearingFromGun = normalRelativeAngleDegrees(absoluteBearing - getGunHeading());

        if (Math.abs(bearingFromGun) <= 3) {
            turnGunRight(bearingFromGun);
            if (getGunHeat() == 0) {
                fire(Math.min(3 - Math.abs(bearingFromGun), getEnergy() - .1));
                ahead(step);
            }
        }
        else {
            turnGunRight(bearingFromGun);
        }
        if (bearingFromGun == 0) {

            scan();
        }
    }

    @Override
    public void onHitWall(HitWallEvent event) {
        back(50);
        turnLeft(45);
    }

    @Override
    public void onHitByBullet(HitByBulletEvent event) {
    }
}
